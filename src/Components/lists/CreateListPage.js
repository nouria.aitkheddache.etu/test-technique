import React, { useState } from 'react';
import {
    Container,
    TextField,
    Button,
    Grid,
} from '@mui/material';
import Header from "../fragment/Header";
import { addListTask } from "../script/core";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';

function CreateListPage() {
    const [nameList, setNameList] = useState("");
    const [tasks, setTasks] = React.useState([]);
    const [taskname, setTaskName] = React.useState("")
    const navigate = useNavigate();
    const [nameListError, setNameListError] = React.useState(false);



    const addTaskToList = () => {
        if (!taskname) {
            toast.error("Veuillez introduire le nom de la tâche");
            return;
        }


        const newTask = {
            nameTask: taskname,
        };

        setTasks((prevState) => [...prevState, newTask]);
        setTaskName("");
    }

    const handleNameListChange = (e) => {
        const value = e.target.value;
        setNameList(value);
        const lettersOnlyRegex = /^[A-Za-z]+$/;
        const isValid = lettersOnlyRegex.test(value);
        setNameListError(!isValid);
    }

    const validate = async () => {
        if (!nameList) {
            toast.error("Veuillez introduire le nom de la liste");
            return;
        }

        if (nameListError) {
            toast.error("Le nom de la liste ne doit contenir que des lettres");
            return;
        }


        const data = {
            name: nameList,
            tasks: tasks
        };
        await addListTask(data)
            .then((res) => {
                if (res.status == 200) {
                    toast.success("Liste a été créée ID : " + res.data);
                    setTimeout(() => {
                        setNameList("");
                        navigate('/');
                    }, 3000);

                } else if (res.status == 409) {
                    toast.error(res.data.data);
                } else {
                    toast.error("Une problème est survenue");
                }
            });
    }


    return (
        <>
            <Header name="Création de liste" />
            <ToastContainer />
            <Container
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    height: "80vh",
                    marginTop: "120px"
                }}
            >
                <TextField
                    label="Nom de la liste"
                    variant="outlined"
                    fullWidth
                    value={nameList}
                    onChange={handleNameListChange}
                    style={{ marginBottom: '16px' }}
                    error={nameListError}
                    helperText={nameListError ? "Le nom ne doit contenir que des lettres" : ""}
                ></TextField>
                <Grid container spacing={2}>
                    <Grid item xs={9}>
                        <TextField
                            label="Nom de la tâche"
                            variant="outlined"
                            fullWidth
                            value={taskname.toString()}
                            onChange={(e) => setTaskName(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={addTaskToList}
                            style={{ width: '100%' }}
                        >
                            Ajouter
                        </Button>
                    </Grid>
                </Grid>

                <Button
                    variant="contained"
                    color="primary"
                    onClick={validate}
                    style={{ marginTop: '20px' }}
                >
                    Sauvegarder
                </Button>
            </Container>
        </>
    );
}

export default CreateListPage;
