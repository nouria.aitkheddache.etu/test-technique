import React, { useState } from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from '@mui/material';
import SearchIcon from "@mui/icons-material/Search";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import Theme from "../design/Theme";
import Header from "../fragment/Header";
import { toast } from "react-toastify";
import { getListTask } from "../script/core";

function DisplayListPage() {
  const [tasks, setTasks] = React.useState([]);
  const [listName, setListName] = React.useState("");
  const [searchValue, setSearchValue] = useState("");
  const theme = Theme().palette;

  const handleSearchClick = async () => {
    try {
      const res = await getListTask(searchValue);
      if (res.status === 200) {
        setTasks(res.data.tasks);
        setListName(res.data.name);
      } else if (res.status === 404) {
        setTasks([]);
        setListName("");
        toast.warning("Aucun résultat trouvé.");
      } else {
        toast.error(res.data);
      }
    } catch (error) {
      console.error(error);
      toast.error("Une erreur s'est produite lors de la list.");
    }
  };



  return (
    <>
      <Header name="Affichage de la liste"/>
      <TableContainer component={Paper} sx={{
        position: "relative",
        bottom: 0,
        margin: "0 auto",
        height: "80vh",
        marginTop: "120px",
        overflowX: "hidden",
        width: "90vw",
        bgcolor: theme.primary.light,
        borderRadius: 1,
        "::-webkit-scrollbar": {
          width: "6px"
        },
        "::-webkit-scrollbar-track": {
          boxShadow: "inset 0 0 6px grey",
        },
        "::-webkit-scrollbar-thumb": {
          background: theme.primary.main,
          height: "2px",
        }
      }}>
        <Table sx={{ minWidth: "60vw" }}>
          <TableHead sx={{
            backgroundColor: theme.primary.main,
            position: "sticky",
            top: 0,
          }}>
            <TableRow>
              <TableCell>
                <b style={{ color: theme.primary.light }}>{listName}</b>
              </TableCell>
              <TableCell></TableCell>
              <TableCell sx={{ textAlign: "right" }}>
                <TextField
                  size="small"
                  variant="outlined"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  sx={{
                    input: { color: theme.primary.light },
                    border: "1px solid white",
                    borderRadius: 1,
                    "& fieldset": { border: "none" },
                  }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchIcon
                          sx={{ color: theme.primary.light, cursor: "pointer" }}
                          onClick={handleSearchClick}
                        />
                      </InputAdornment>
                    ),
                  }}
                ></TextField>
              </TableCell>
              
            </TableRow>
          </TableHead>
          <TableBody>
            {tasks.map((row) => (
              <TableRow
                key={row.taskId}
                sx={{ ":hover": { background: theme.secondary.light } }}
              >
                <TableCell >
                  {row.nameTask}
                </TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
       
    </>
  );
}

export default DisplayListPage;
