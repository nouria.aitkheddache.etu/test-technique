
import {  getAPI, postAPI } from "./utils";
const API_URL = `http://localhost:9006/api`

export const addListTask = (listTask) => {
    return postAPI(API_URL, "/listTask", listTask)
}

export const getListTask = (listTaskId ) => {
    return getAPI(API_URL, "/listTask/", listTaskId)
}