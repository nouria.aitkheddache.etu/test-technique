import axios from "axios";

const api = axios.create();
export const postAPI = async (baseUrl, url, data)=> {
    return await api({
        method: 'post',
        url: `${baseUrl}${url}`,
        data
    }).then((response) => {
        console.log(response)
        return {
            status: response.status,
            data: response.data
        }
    }).catch((error) => {
        console.log(error)
        return {
            status: error.response.status,
            data: error.response.data
        }
    })
}


export const getAPI = async (baseUrl, url, data) => {
    return await api({
        method: 'get',
        url: `${baseUrl}${url}${data}`,
    }).then((response) => {
        console.log(response)
        return {
            status: response.status,
            data: response.data
        }
    }).catch((error) => {
        console.log(error)
        return {
            status: error.response.status,
            data: error.response.data
        }
    })
}



