
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import React from 'react';
import Theme from "../design/Theme";

const Header = ({name}) => {

    const theme = Theme().palette
   
    return(
        <Box>
            <AppBar sx={{background: theme.primary.main, display: 'flex', flexDirection: 'column',}}>
                <Toolbar sx={{display : "flex"}}>
                    <Typography variant="h6">{name}</Typography>
                    <Typography sx={{flexGrow : 1}}></Typography>
                </Toolbar>
            </AppBar>
          
        </Box>

    )
}

export default Header