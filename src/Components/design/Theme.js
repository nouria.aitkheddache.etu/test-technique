import { createTheme } from "@mui/material/styles";

const Theme = () =>
  createTheme({
    palette: {
      primary: {
        main: "#601113",
        light: "#FFFFFF",
        dark: "#000000"
      },
      secondary: {
        main: "#ee964b",
      },
    },
  });

export default Theme;
