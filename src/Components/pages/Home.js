
import Header from "../fragment/Header";
import React from 'react';

import { Link } from 'react-router-dom';
import {Button} from '@mui/material';
import Box from '@mui/material/Box';
function HomePage() {
  return (
    <>
    <Header name="Accueil"/>
    <Box
                       sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        minHeight: '100vh',
                      }} >
      
      
      <Button sx={{ mb: 2 }} component={Link} to="/CreateListPage" variant="contained" color="primary">
       Créer une liste
       </Button>
      <Button sx={{ mb: 2 }}  component={Link} to="/DisplayListPage" variant="contained" color="primary">
        Afficher une liste
      </Button>
      </Box>
    </>
  );
}

export default HomePage;
