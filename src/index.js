import React from 'react';
import './index.css';
import reportWebVitals from './reportWebVitals';
import ReactDOM from 'react-dom/client'
import './index.css'
import { createBrowserRouter, RouterProvider } from "react-router-dom"
import Theme from "./Components/design/Theme"
import Home from "./Components/pages/Home"
import CreateListPage from "./Components/lists/CreateListPage"
import DisplayListPage from "./Components/lists/DisplayListPage"
import { ThemeProvider } from '@emotion/react'
const router = createBrowserRouter([
  {
      path: "/",
      element: <Home/>,
  },
  {
    path: '/CreateListPage',
    element: <CreateListPage/>,
},
{
  path: '/DisplayListPage',
  element: <DisplayListPage/>,
},

])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
   <ThemeProvider theme={Theme}>
        <RouterProvider router={router} />
      </ThemeProvider>
  </React.StrictMode>
);


reportWebVitals();
